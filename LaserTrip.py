import RPi.GPIO as GPIO
import sys
from picamera import PiCamera
import time
import os.path
import shutil
from datetime import datetime
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from S3 import upload_to_aws

fromaddr = "bishthemantsingh05@gmail.com"
toaddr = "hbisht2@illinois.edu"
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

# time.sleep(2)
GPIO.cleanup()
# sys.exit()
##LDR Working Code Start
ldr = 7
# triggerPIN = 14
triggerPIN = 8
# print(ldr)
GPIO.setup(triggerPIN,GPIO.OUT)
buzzer = GPIO.PWM(triggerPIN, 800) 

while True:
    # print("ldr pin is  ", ldr)
    GPIO.setup(ldr,GPIO.OUT)
    # print("Setup Output Pin")
    GPIO.output(ldr,GPIO.LOW)
    # print("Setup Output to Low")
    time.sleep(0.1)
    GPIO.setup(ldr,GPIO.IN)
    # print("Setup LDR as Input Pin")
    currentTime=time.time()
    # print("Current Time is :")
    # print(currentTime)
    # time.sleep(2)
    diff = 0
    # count = 0
    # print(diff)
    while (GPIO.input(ldr) == GPIO.LOW):

        #print("Start Calculating the Diff")
        #print(time.time())
        diff = (time.time() - currentTime)*1000
        # count +=1
        # print("count is:", count)
        # print("Diff inside the While Loop: ", diff)
        if diff > 45:
        
            # print("Diff inside the if condtion : ", diff)
            print("Buzzer Start")
            buzzer.start(80)  
            now = datetime.now()
            file_exists = os.path.exists('Intruder.JPG')
            # print("Destination is : ", Destination)
            # print("file Exists: ",file_exists)
            if file_exists:
                print("Image Existing hence Archiving the file")
                source = "Intruder.JPG"
                filename= "Intruder" + str(now)+".JPG"
                print("Destination Filename is : ", filename)
                Destination = "Archive/"+ filename
                s3_source= "/home/pi/textdetection/"+ source
                print("s3_source is : ", s3_source)
                uploaded= upload_to_aws(s3_source, 'vamsi-cs437-final', filename)
                    
                print("Image Uploaded:", uploaded)
                print("Local Archive Location is :", Destination)
                time.sleep(1)
                shutil.move(source ,Destination)
                
            camera = PiCamera()
            camera.resolution = (1920, 1080)
            camera.framerate = 15
            #camera.vflip = True
            #camera.hflip = True
            camera.start_preview()
            #time.sleep(0.5)
            print("Taking a Picture")
            camera.capture("Intruder.JPG")
            camera.close()
            print("Picture taken")
            #time.sleep(0.5)
            print("Sending Email")
            msg = MIMEMultipart()
            msg['From'] = fromaddr
            msg['To'] = toaddr
            msg['Subject'] = "INTRUDER ALERT!!!"
            body = "ATTENTION! There is an Intruder. Attached is the Picture of the Intruder"
            msg.attach(MIMEText(body, 'plain'))
            filename = "Intruder.JPG"
            attachment = open("/home/pi/textdetection/Intruder.JPG", "rb")
            # filename = "Intruder2021-12-10 11:17:29.963528.JPG"
            # attachment = open("/home/pi/textdetection/Archive/Intruder2021-12-10 11:17:29.963528.JPG", "rb")
            part = MIMEBase('application', 'octet-stream')
            part.set_payload((attachment).read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
            msg.attach(part)
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.starttls()
            server.login(fromaddr, "Oct@198709")
            text = msg.as_string()
            server.sendmail(fromaddr, toaddr, text)
            server.quit()
            print("email Sent")
            #time.sleep(1)
        else:
            
            buzzer.stop()



